Python-dvdvideo
===============

Python library written by Bastian Blank to make backup copies of DVDs.
Based on https://tracker.debian.org/pkg/python-dvdvideo .

Maintained on https://codeberg.org/pere/python-dvdvideo .
